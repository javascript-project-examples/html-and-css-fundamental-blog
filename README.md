# HTML and CSS - Fundamental Blog

A simple Blog website using HTML, CSS and a tiny bit of JS.

## Name
Fundamental Blog

## Description
A simple blog website using basic HTML, CSS and JavaScript. The project uses Bootstrap 5 for styling.


## Authors and acknowledgment
Noroff Accelerate

### Special Thanks to the Third-party libraries
Bootstrap 5 [getbootstrap.com](https://getbootstrap.com/)






